const Porta = "10080";
// requisição do BODYPARSER
const bodyParser = require('body-parser');
const express = require('express');
const server = express();
const cors = require('cors');

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

server.use(cors);

server.listen(process.env.PORT || Porta, function() {
    console.log('Servidor ativo na porta :' + Porta);
});

module.exports = server;