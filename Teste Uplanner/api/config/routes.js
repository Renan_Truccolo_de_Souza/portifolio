const express = require('express');

module.exports = function(server) {
    //Rotas da api

    const router = express.Router();
    server.use('/api', router);

    //registrando metodos da API no router

    const contaService = require('../endpoints/contaBancaria/contaService')
    contaService.register(router, '/contaBancaria')
    const transacaoService = require('../endpoints/transacao/transacaoService')
    transacaoService.register(router, '/transacao')

}