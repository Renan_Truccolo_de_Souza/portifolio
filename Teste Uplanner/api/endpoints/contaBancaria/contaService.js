const contaBancaria = require('./conta');

contaBancaria.methods(['get', 'post', 'put', 'delete']);

contaBancaria.updateOptions({ new: true, runValidators: true});

module.exports = contaBancaria;