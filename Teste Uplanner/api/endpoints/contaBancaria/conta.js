const restful = require('node-restful');
const mongoose = restful.mongoose;

const contaBancariaSchema = new mongoose.Schema({
    contanome: { type: String, required: true },
    status: { type: String, required: true},
    saldo: { type: String, required: true},
    
});

module.exports = restful.model('contaBancaria', contaBancariaSchema);