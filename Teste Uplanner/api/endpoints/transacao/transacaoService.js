const transacao = require('./transacao');

transacao.methods(['get', 'post', 'put', 'delete']);

transacao.updateOptions({ new: true, runValidators: true});

module.exports = transacao;