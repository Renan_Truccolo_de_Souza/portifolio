const restful = require('node-restful');
const mongoose = restful.mongoose;

const transacaoSchema = new mongoose.Schema({
    nomeTransacao: { type: String, required: true },
    valor: { type: String, required: true},
    conta: { type: String, require: true}
    
});

module.exports = restful.model('transacao', transacaoSchema);