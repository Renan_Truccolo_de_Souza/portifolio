import axios from 'axios'

const client = axios.create({
  baseURL: 'http://localhost:10080',
  json: true
})

export default {
  async execute (method, resource, data) {
    // inject the accessToken for each request
    console.log(data);
    return client({
      method,
      url: resource,
      data,
    }).then(req => {
      return req.data
    })
  },
  getTransacao () {
    return this.execute('GET', '/api/transacao')
  },
  getTrasacaoID (id) {
    return this.execute('GET', `/api/transacao/${id}`)
  },
  createTransacao (data) {
    return this.execute('POST', '/api/transacao', data)
  },
  updateTransacao (id, data) {
    return this.execute('PUT', `/api/transacao/${id}`, data)
  },
  deleteTransacao (id) {
    return this.execute('DELETE', `/api/transacao/${id}`)
  },

  getConta () {
    return this.execute('GET', '/api/contaBancaria')
  },
  getContaID (id) {
    return this.execute('GET', `/api/contaBancaria/${id}`)
  },
  createConta (data) {
    return this.execute('POST', '/api/contaBancaria', data)
  },
  updateConta (id, data) {
    return this.execute('PUT', `/api/contaBancaria/${id}`, data)
  },
  deleteConta (id) {
    return this.execute('DELETE', `/api/contaBancaria/${id}`)
  }
}