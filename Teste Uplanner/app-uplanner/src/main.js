import Vue from 'vue'
import App from './App.vue'
import router from './router'
import firebase from 'firebase'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);
Vue.config.productionTip = false

//firebase
var firebaseConfig = {
  apiKey: "AIzaSyCjmMEkPeQsuUZwZ7uBcvviocTFriYis10",
  authDomain: "testeuplanner.firebaseapp.com",
  projectId: "testeuplanner",
  storageBucket: "testeuplanner.appspot.com",
  messagingSenderId: "698978366626",
  appId: "1:698978366626:web:d4a762376b16f8159653fb",
  measurementId: "G-XEVERRWDNB"
};
firebase.initializeApp(firebaseConfig)


let app = '';
firebase.auth().onAuthStateChanged(()=> {
  if(!app){
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')
  }
})

