import Vue from 'vue';
import Router from 'vue-router';
import firebase from 'firebase';

import Home from '../views/Home.vue';
import Login from '../views/Login';
import SignUp from '../views/SignUp';
import Transacoes from '../views/Transacoes';
import about from '../views/About'

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/registrar',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: "/Transacoes",
      name: "Transaçoes",
      component: Transacoes,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/About',
      name: 'About',
      component: about,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home,
      meta: {
        requiresAuth: true
      },
    }
  ]
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next('login');
  else if (!requiresAuth && currentUser) next('home');
  else next();
});

export default router;